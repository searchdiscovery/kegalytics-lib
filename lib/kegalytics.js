var gpio = require("pi-gpio");
var KegMeter = require('./kegmeter');

var pins = {
  flow: [16,17]
};

var beers = pins.flow.map(function (pin) {
  return new KegMeter(pin);
});

beers.forEach(function (beer) {
  beer.on('pour:start', function () {
    console.log('pour:start');
  });

  beer.on('pour:progress', function () {
    console.log('pour:progress');
  });

  beer.on('pour:end', function () {
    console.log('pour:end');
  });
});
