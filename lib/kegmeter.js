var FlowMeter = require('./flowmeter');

/*
settings {

}
*/

var Keg = function (contents, meter) {
  this.contents = contents;
  this.remaining = this.contents.volume;
  this.meter = meter.reset();

  this.meter.on('flow', this.setFlow.bind(this));
};

Keg.prototype.setFlow = (function () {
  var that = this,
      timeout;

  return function (flow, hertz) {
    if (!that._isFlowing) {
      that._isFlowing = true;
      that.emit('pour:start');
    }
    that.emit('pour:progress', flow, hertz);
    that.remaining = that.contents.volume - flow;
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      that._isFlowing = false;
      that.emit('pour:end', flow);
    }, 2000);
  };
})();

Keg.prototype.getFlow = function () {
  return this._isFlowing;
};

