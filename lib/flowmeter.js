// flow meter
var gpio = require('pi-gpio'),
    util = require('util'),
    EventEmitter = require('events').EventEmitter;

function roundToDecimals(int, places) {
  var x = Math.pow(10, places);
  return Math.round(int * x) / x;
}

// Utility function to poll a pin's value
function monitorPin (pin, cb) {
  gpio.read(pin, function (err, value) {
    if (err) {
      console.error(err);
      return;
    }
    cb(value);
    monitorPin(pin, cb);
  });
}

function FlowMeter (pin) {
  this.lastPinChange = new Date().getTime();
  this._pinState = null;
  this.hertz = 0;
  this.flow = 0;
  this.pin = pin;

  EventEmitter.call(this);

  monitorPin(pin, this.readPinState.bind(this));
}
util.inherits(FlowMeter, EventEmitter);

// Over time, the flow value (which indicates how much how liquid has flown
// through the meter) will accumulate. The reset method clears this value.
FlowMeter.prototype.reset = function () {
  this.hertz = 0;
  this.flow = 0;
  return this;
};

FlowMeter.prototype.readPinState = function (val) {
  val = !!val;
  var now = new Date().getTime();

  // if pinstate has not been set yet
  if (this._pinState === null) {
    this._pinState = val;
  }
  // if the pinState changes from low to high
  if (val && this._pinState !== val) {
    var pinChange = now,
        pinDelta = pinChange - this.lastPinChange;
    if (pinDelta < 1000) {
      this.hertz = roundToDecimals(1000 / pinDelta, 3);
      this.flow = roundToDecimals(this.hertz / (60 * 7.5), 3);
    }
    this.lastPinChange = pinChange;
    this.emit('flow', this.flow, this.hertz);
  } else if (now - this.lastPinChange > 1000) {
    this.hertz = 0;
  }
  this._pinState = val;
};

module.exports = FlowMeter;
